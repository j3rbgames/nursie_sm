#!/usr/bin/env python

import rospy
import smach
import time
from smach import State, StateMachine
from smach_ros import IntrospectionServer

import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from tf.transformations import quaternion_from_euler
from collections import OrderedDict
from nursie_msgs.srv import Ubicacion_Msg, Ubicacion_MsgResponse

# Puntos de la casa
waypoints = [
    ['base', [-3.0, 1.0, 0.0], [0.0, 0.0, 1.0, 0.0]],
    ['mostrador', [3.0, 3.0, 0.0], [0.0, 0.0, 1.0, 0.0]],
    ['habitacion1', [-6.0, 0.0, 0.0], [0.0, 0.0, 1.0, 0.0]],
    ['habitacion2', [-3.0, 1.0, 0.0], [0.0, 0.0, 1.0, 0.0]],
    ['habitacion3', [1.0, 2.0, 0.0], [0.0, 0.0, 1.0, 0.0]],
    ['habitacion4', [-6.0, 5.0, 0.0], [0.0, 0.0, 1.0, 0.0]]
]


class Inicio(State):
    def __init__(self):  # Datos de entrada  #Datos de salida
        State.__init__(self, outcomes=['1', '0', 'repeat'], input_keys=[], output_keys=[])

    def execute(self, userdata):
        # Codigo que tenga que hacer el estado
        time.sleep(1)
        rospy.loginfo("Ejecucion inicio")
        return '1'


class EsperarOrden(State):
    def __init__(self):
        State.__init__(self, outcomes=['1', '0'], input_keys=['input'], output_keys=['output'])


    def execute(self, userdata):
        time.sleep(1)
        rospy.loginfo("Ejecucion Esperarorden")
        return '1'


class HacerRuta(State):
    def __init__(self):
        State.__init__(self, outcomes=['1', '0', 'repeat'], input_keys=['ubi', 'repeat'], output_keys=['output'])
        self._move_base = actionlib.SimpleActionClient("/move_base", MoveBaseAction)
        rospy.loginfo("Activando el cliente de navegacion..")
        self._move_base.wait_for_server(rospy.Duration(15))
        

    def execute(self, userdata):
        rospy.loginfo("Ejecucion HacerRuta")
        rospy.loginfo(userdata.repeat)
        rospy.loginfo(userdata.ubi)
        time.sleep(1)
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'

        global waypoints
        
        if userdata.repeat == 0:
            
            goal.target_pose.pose.position.x = waypoints[userdata.ubi][1][0]
            goal.target_pose.pose.position.y = waypoints[userdata.ubi][1][1]
            goal.target_pose.pose.position.z = waypoints[userdata.ubi][1][2]
            goal.target_pose.pose.orientation.x = waypoints[userdata.ubi][2][0]
            goal.target_pose.pose.orientation.y = waypoints[userdata.ubi][2][1]
            goal.target_pose.pose.orientation.z = waypoints[userdata.ubi][2][2]
            goal.target_pose.pose.orientation.w = waypoints[userdata.ubi][2][3]

        else:
            goal.target_pose.pose.position.x = waypoints[0][1][0]
            goal.target_pose.pose.position.y = waypoints[0][1][1]
            goal.target_pose.pose.position.z = waypoints[0][1][2]
            goal.target_pose.pose.orientation.x = waypoints[0][2][0]
            goal.target_pose.pose.orientation.y = waypoints[0][2][1]
            goal.target_pose.pose.orientation.z = waypoints[0][2][2]
            goal.target_pose.pose.orientation.w = waypoints[0][2][3]

        rospy.loginfo(goal)

        # sends the goal
        self._move_base.send_goal(goal)
        self._move_base.wait_for_result()
        # Comprobamos el estado de la navegacion
        nav_state = self._move_base.get_state()

        
        if nav_state == 3:
            # Si el estado es correcto pero aun no ha vuelto input valdra 0, si la ruta era la de vuelta, valdra 1
            if userdata.repeat == 0:
                userdata.output = 1
                return 'repeat'
            else:
                userdata.output = 0
                return '1'
        else:
            return '0'


def callbackServicio(data):
    rospy.loginfo("Se ha llamado al servicio /nursie_navigation_fsm")
    sm = StateMachine(outcomes=['succeeded', 'aborted'])
    sm.userdata.sm_ubi = data.ubicacion
    sm.userdata.sm_repeat = 0

    with sm:
        StateMachine.add('Inicio', Inicio(), transitions={'1': 'EsperarOrden', '0': 'Inicio', 'repeat': 'EsperarOrden'}, 
        remapping={'input': 'bateria', 'output': 'bateria'})

        StateMachine.add('EsperarOrden', EsperarOrden(), 
        transitions={'1': 'HacerRuta', '0': 'aborted'}, 
        remapping={'input': 'sm_ubi', 'output': 'sm_repeat'})

        # HacerRuta
        StateMachine.add('HacerRuta', HacerRuta(),transitions={'1': 'succeeded', '0': 'aborted', 'repeat': 'HacerRuta'},
        remapping={'ubi': 'sm_ubi', 'repeat': 'sm_repeat', 'output': 'sm_repeat'})


    sis = IntrospectionServer('server_name', sm, '/SM_ROOT')
    sis.start()
    # Ejecutamos la maquina de estados
    sm_ejecutada = sm.execute()
    rospy.spin()
    sis.stop()

    respuesta = Ubicacion_MsgResponse()
    respuesta.response = True
    return respuesta


# Servicio
rospy.init_node('nursie_navigation_fsm', anonymous=False)
servicio = rospy.Service('/nursie_navigation_fsm',
                         Ubicacion_Msg, callbackServicio)
rospy.Rate(1)
rospy.loginfo("El servicio esta listo")
rospy.spin()

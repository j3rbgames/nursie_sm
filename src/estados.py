#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Modulo que contiene todos los estados de la maquina de estados
Estos estados son cargados por la maquina de estados que se encara de la iteracion 
entre ellos segun responda el entorno
"""

# ---------------------------- Importar librerías ---------------------------- #

import rospy
import smach
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import os

from pygame import mixer
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from playsound import playsound

# ---------------------------------------------------------------------------- #


# ------------------------------ Estado INICIAR ------------------------------ #
class Iniciar(smach.State):
    """
    Estado para inicializar el robot, al terminar pasa al estado ESTADO_BASE y queda esperando órdenes.
    """

    def __init__(self):
        # Your state initialization goes here
        smach.State.__init__(
            self, outcomes=['inicio_hecho'],)

    def execute(self, userdata):
        # Your state execution goes here
        rospy.loginfo("En el estado INICIO")
        # TODO: Inicializar todo lo necesario
        # TODO: Mover el robot a su posición habitual
        rospy.sleep(2)
        rospy.loginfo("Estado INICIO finalizado")
        return 'inicio_hecho'
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
class CogerCarga(smach.State):
    """
    En este estado el robot haria la funcion de recoger la comida o la carga que se le asignara
    Una vez se ha completado el estado pasara al estado de IR_A_HABITACION
    
    IR_A_COCINA -> COGER_CARGA -> succeeded': 'IR_A_HABITACION
    """

    def __init__(self):
        smach.State.__init__(
            self, outcomes=['succeeded'])

    def execute(self, userdata):
        rospy.loginfo("Recogiendo comida")
        rospy.sleep(1)
        cont = 1
        if cont == 1:
            return 'succeeded'
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
class DejarCarga(smach.State):
    """
    En este estado el robot haria la funcion de descargar la comida o la carga que se le asignara
    Una vez se ha completado el estado continuara con la ruta
    
    DETECTAR_CARA -> DEJAR_CARGA -> succeeded': 'continue
    """
    def __init__(self):
        smach.State.__init__(self, outcomes=['succeeded'])

    def execute(self, userdata):
        rospy.loginfo("Dejando la comida")
        for cont in range(0, 3):
            cont += 1
            rospy.sleep(1)
            print(".")
        if cont > 0:
            return 'succeeded'

# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
class DetectarCara(smach.State):
    """
    En este estado el robot recoge las imagenes captadar por la camara y 
    compara para comprobar si hay en rostro en la imagen, en caso afirmtativo cambia 
    al estado Dejar_carga

    IR_A_HABITACION -> DETECTAR_CARA -> (succeeded': 'DEJAR_CARGA, preempted': 'DETECTAR_CARA')
    """
    def __init__(self):
        smach.State.__init__(self, outcomes=['succeeded', 'preempted'])
        self.bridge_Object = CvBridge()
        self.image_sub = None
        self.imagen_captada = False
        self.cv_image = None
        self.imagen_reenvio = None

    def camera_callback(self, data):
        print("Dentro de callback")
        try:
            self.cv_image = self.bridge_Object.imgmsg_to_cv2(data, desired_encoding="bgr8")
        except CvBridgeError as e:
            print(e)
        
        print("DEspues del try")

        # Obtenemos las dimensiones de la imagen capturada
        height, width, channels = self.cv_image.shape

        # Definimos el clasificador
        ruta = os.path.dirname(os.path.abspath(__file__))+"/clasificadores/haarcascade_frontalface_default.xml"
        
        cascada = cv2.CascadeClassifier(ruta)
        print("Despues de obtener la cascada ", cascada)
        # convertir la imagen a escala de grises
        img_gris = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)
        print(img_gris)

        cascada = cascada.detectMultiScale(img_gris, 1.1, 5)
        control = False
        for x, y, w, h in cascada:
            print("DEntro del for, se ha encontrado cara")
            cv2.rectangle(self.cv_image, (x,y), (x+w, y+h), (255,0,0),3)
            self.imagen_captada = True
            self.imagen_reenvio = self.bridge_Object.cv2_to_imgmsg(self.cv_image)
        #Hace de if, comprueba si las variables x e y han sido decraradas, 
        # de ser asi sigifica que se ha detectado alguna cara y por tanto se ha dibujado el cuadro


    def execute(self, userdata):
        ruta = os.path.dirname(os.path.abspath(__file__))

        #mixer.init()
        #mixer.music.load(ruta + '/saludo.mp3')
        #mixer.music.play()
        rospy.loginfo("Buscando rostros")
        self.image_sub = rospy.Subscriber("turtlebot3/camera/image_raw", Image, 
                                            self.camera_callback)

        rospy.sleep(2)
        if self.imagen_captada:
            my_pub = rospy.Publisher('/img_captured', Image, queue_size=1)
            my_pub.publish(self.imagen_reenvio)
            self.image_sub.unregister()
            return 'succeeded'
        else:
            self.image_sub.unregister()
            return 'preempted'

# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
waypoints = {
    'HABITACION_1': ((1.285834, -1.25711, 0.0),  (0.0, 0.0, 1.0, -0.200424)),
    'HABITACION_2': ((1.403263, -2.98571, 0.0),  (0.0, 0.0, 1.0, 0.052814)),
    'HABITACION_3': ((1.372873, -4.58602, 0.0),  (0.0, 0.0, 1.0, 0.186391)),
    'HABITACION_4': ((1.487318, -6.439454, 0.0),  (0.0, 0.0, 1.0, 0.044397)),
    'COCINA':       ((-3.0, -6.0, 0.0), (0.0, 0.0, 1.0, 0.0)),
    'BASE':         ((-3.0, -1.0, 0.0), (0.0, 0.0, 1.0, 0.0))
}
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
class IrA(SimpleActionState):
    """
    En este estado el robot se dirigira al punto que se le ha indicado, cuando llegue al punto
    dependiendo de en que habitacion se encuentre podra pasar a un estado u otro.
    
    Inicio del reparto -> IR_A_COCINA -> COGER_CARGA

    COGER_CARGA -> IR_A_HABITACION -> DETECTAR_CARA
    """

    def __init__(self, destino):
        SimpleActionState.__init__(
            self, 'move_base', MoveBaseAction,
            input_keys=['data_in', 'index'],
            goal_cb=self.__goal_cb,
            outcomes=['succeeded', 'aborted', 'preempted'])
        self.destino = destino

    def __goal_cb(self, userdata, old_goal):
        rospy.loginfo("CALLBACK: Ejecutando ir a ...")
        destino = ""
        if self.destino == 'HABITACION':
            # print("==============================")
            # print("DESTINOS:", userdata.data_in)
            # print("INDEX:", userdata.index)
            # print("DATOS:", userdata.data_in.destinos)
            # print("==============================")
            destino = "HABITACION_" + str(ord(userdata.data_in.destinos[userdata.index]))
            # destino = "HABITACION_1"
            print("==============================")
            print("HABITACION:", destino)
            print("==============================")
        else:
            destino = self.destino
            pass

        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()

        goal.target_pose.pose.position.x = waypoints[destino][0][0]
        goal.target_pose.pose.position.y = waypoints[destino][0][1]
        goal.target_pose.pose.position.z = waypoints[destino][0][2]
        goal.target_pose.pose.orientation.x = waypoints[destino][1][0]
        goal.target_pose.pose.orientation.y = waypoints[destino][1][1]
        goal.target_pose.pose.orientation.z = waypoints[destino][1][2]
        goal.target_pose.pose.orientation.w = waypoints[destino][1][3]

        return goal
# ---------------------------------------------------------------------------- #

# ---------------------------------------------------------------------------- #
# ---------------------------------------------------------------------------- #

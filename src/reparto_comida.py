#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Modulo que contiene la definicion de la máquina de estados, donde se ejecutan los estados.
"""

# ---------------------------- Importar librerías ---------------------------- #
import rospy
import smach
import smach_ros
import estados
import os


from smach_ros import ActionServerWrapper
from nursie_msgs.msg import OrdenRepartoAction, OrdenRepartoFeedback, OrdenRepartoResult
from pygame import mixer
# ---------------------------------------------------------------------------- #



# -------------------------- Función que crea la SM -------------------------- #
def construir_sm():
    ruta = os.path.dirname(os.path.abspath(__file__))

    #mixer.init()
    #mixer.music.load(ruta + '/saludo.mp3')
    #mixer.music.play()

    """
    Función que se encarga de construir la máquina de estados, no recibe nada y devuelve la propia máquina de estados
    La máquina de estados está constituida por los estados:
    INICIO, IR_A_COCINA, COGER_CARGA, IR_A_HABITACION, DETECTAR_CARA, DEJAR_CARGA e IR_A_BASE

    Se trata de una maquina de estados anidada donde el iterador itera con los estados:
    IR_A_COCINA, COGER_CARGA, IR_A_HABITACION, DETECTAR_CARA, DEJAR_CARGA
    """
    # Construct state machine
    sm = smach.StateMachine(
        outcomes=['succeeded', 'aborted', 'preempted'], input_keys=['destinos'])
    # sm.userdata.habitacion = None

    with sm:
        # Add states in here...
        smach.StateMachine.add('INICIO', estados.Iniciar(),
                               transitions={'inicio_hecho': 'ITERADOR_SM'})

        iterador_sm = smach.Iterator(outcomes=['succeeded', 'preempted', 'aborted'],
                                     input_keys=['destinos'],
                                     it=lambda: range(0, len(sm.userdata.destinos.destinos)),
                                     output_keys=['destinos'],
                                     it_label='index',
                                     exhausted_outcome='succeeded')

        with iterador_sm:
            contenedor_sm = smach.StateMachine(outcomes=['succeeded', 'preempted', 'aborted', 'continue'],
                                               input_keys=['destinos', 'index'],
                                               # output_keys=['even_nums', 'odd_nums']
                                               )

            with contenedor_sm:


                smach.StateMachine.add('IR_A_COCINA', estados.IrA('COCINA'),
                                       transitions={'succeeded': 'COGER_CARGA'},
                                       remapping={'data_in': 'destinos'})

                smach.StateMachine.add('COGER_CARGA', estados.CogerCarga(),
                                       transitions={'succeeded': 'IR_A_HABITACION'})

                smach.StateMachine.add('IR_A_HABITACION', estados.IrA('HABITACION'),
                                       transitions={'succeeded': 'DETECTAR_CARA'},
                                       remapping={'data_in': 'destinos'})

                smach.StateMachine.add('DETECTAR_CARA', estados.DetectarCara(),
                                       transitions={'succeeded': 'DEJAR_CARGA',
                                                    'preempted': 'DETECTAR_CARA'})

                smach.StateMachine.add('DEJAR_CARGA', estados.DejarCarga(),
                                       transitions={'succeeded': 'continue'})

            # close contenedor_sm
            smach.Iterator.set_contained_state('CONTENEDOR_SM', contenedor_sm,
                                               loop_outcomes=['continue'])

        # close iterador_sm
        smach.StateMachine.add('ITERADOR_SM', iterador_sm,
                               {'succeeded': 'IR_A_BASE', 'aborted': 'aborted'})

        smach.StateMachine.add('IR_A_BASE', estados.IrA('BASE'),
                            transitions={'succeeded': 'succeeded'},
                            remapping={'data_in': 'destinos'})

    return sm
# ---------------------------------------------------------------------------- #


# ------------------------------ Función Main() ------------------------------ #
def main():
    """ En main se crea un nodo y una accion para la comunicacion con el cliente"""

    rospy.loginfo("Iniciado nodo reparto_comida")
    rospy.init_node('reparto_comida', anonymous=True)

    # Construct a state machine
    sm = construir_sm()

    # Create and start the introspection server
    sis = smach_ros.IntrospectionServer('reparto_comida', sm, '/SM_ROOT')
    sis.start()

    # Construct action server wrapper
    asw = ActionServerWrapper(
        server_name='reparto_comida_servidor',
        action_spec=OrdenRepartoAction,
        wrapped_container=sm,
        succeeded_outcomes=['succeeded'],
        aborted_outcomes=['aborted'],
        preempted_outcomes=['preempted'],
        goal_key='destinos',
    )

    # Run the server in a background thread
    asw.run_server()

    # Wait for control-c
    rospy.spin()
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        rospy.loginfo("Nodo reparto_comida finalizado")
# ---------------------------------------------------------------------------- #


# ---------------------------------------------------------------------------- #
# ---------------------------------------------------------------------------- #

#! /usr/bin/env python
# -*- coding: utf-8 -*-

import roslib
import rospy
import actionlib

from nursie_msgs.msg import OrdenRepartoAction, OrdenRepartoGoal

if __name__ == '__main__':
    rospy.init_node('reparto_comida_cliente')
    client = actionlib.SimpleActionClient(
        'reparto_comida_servidor', OrdenRepartoAction)
    client.wait_for_server()

    goal = OrdenRepartoGoal()
    goal.destinos = [1, 2, 3, 4]

    # Fill in the goal here
    client.send_goal(goal)
    client.wait_for_result(rospy.Duration.from_sec(5.0))

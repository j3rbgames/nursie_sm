#!/usr/bin/env python

import rospy
import smach
import time
from smach import State, StateMachine
from smach_ros import IntrospectionServer

import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from tf.transformations import quaternion_from_euler
from collections import OrderedDict
from nursie_msgs.srv import Ubicacion_Msg, Ubicacion_MsgResponse

# Puntos de la casa
waypoints = [
    ['base', (1.0, 3.0, 0.0), (0.0, 0.0, 1.0, 0.0)],
    ['mostrador', (3.0, 3.0, 0.0), (0.0, 0.0, 1.0, 0.0)],
    ['habitacion1', (-6.0, 0.0, 0.0), (0.0, 0.0, 1.0, 0.0)],
    ['habitacion2', (-3.0, 1.0, 0.0), (0.0, 0.0, 1.0, 0.0)],
    ['habitacion3', (-6.0, 5.0, 0.0), (0.0, 0.0, 1.0, 0.0)],
    ['habitacion4', (-6.0, 5.0, 0.0), (0.0, 0.0, 1.0, 0.0)]
]


class Inicio(State):
    def __init__(self):  # Datos de entrada  #Datos de salida
        State.__init__(self, outcomes=['1', '0', 'repeat'], input_keys=[], output_keys=[])

    def execute(self, userdata):
        # Codigo que tenga que hacer el estado
        time.sleep(1)
        return '1'


class EsperarOrden(State):
    def __init__(self):
        State.__init__(self, outcomes=['base', 'mostrador', 'habitacion1', 'habitacion2',
                                       'habitacion3', 'habitacion4', 'aborted'], input_keys=['input'], output_keys=[''])

    def execute(self, userdata):
        if userdata.input == 0:
            return 'base'
        elif userdata.input == 1:
            return 'mostrador'
        elif userdata.input == 2:
            return 'habitacion1'
        elif userdata.input == 3:
            return 'habitacion2'
        elif userdata.input == 4:
            return 'habitacion3'
        elif userdata.input == 5:
            return 'habitacion4'
        else:
            return 'aborted'


class HacerRuta(State):
    def __init__(self, posicion, orientacion, lugar):
        State.__init__(self, outcomes=['1', '0', 'repeat'], input_keys=[
                       'input'], output_keys=['output'])
        self._posicion = posicion
        self._orientacion = orientacion
        self._lugar = lugar
        self._move_base = actionlib.SimpleActionClient(
            "/move_base", MoveBaseAction)
        rospy.loginfo("Activando el cliente de navegacion..")
        self._move_base.wait_for_server(rospy.Duration(15))

    def execute(self, userdata):
        time.sleep(1)
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        rospy.loginfo(self._posicion)
        goal.target_pose.pose.posicion.x = self._posicion[0]
        goal.target_pose.pose.posicion.y = self._posicion[1]
        goal.target_pose.pose.posicion.z = self._posicion[2]
        goal.target_pose.pose.orientacion.x = self._orientacion[0]
        goal.target_pose.pose.orientacion.y = self._orientacion[1]
        goal.target_pose.pose.orientacion.z = self._orientacion[2]
        goal.target_pose.pose.orientacion.w = self._orientacion[3]

        # sends the goal
        self._move_base.send_goal(goal)
        self._move_base.wait_for_result()
        # Comprobamos el estado de la navegacion
        nav_state = self._move_base.get_state()

        
        if nav_state == 3:
            # Si el estado es correcto pero aun no ha vuelto input valdra 0, si la ruta era la de vuelta, valdra 1
            if userdata.input == 0:
                userdata.output = 1
                return 'repeat'
            else:
                return '1'
        else:
            return '0'


class main():
    def __init__(self, lugarDestino):

        self.lugarDestino = lugarDestino

        #rospy.init_node('move_base_action_client', anonymous=False)

        sm = StateMachine(outcomes=['succeeded', 'aborted'])
        sm.userdata.sm_input = self.lugarDestino

        with sm:
            StateMachine.add('Inicio', Inicio(), transitions={'1': 'EsperarOrden', '0': 'Inicio', 'repeat': 'EsperarOrden'}, 
            remapping={'input': 'bateria', 'output': 'bateria'})

            StateMachine.add('EsperarOrden', EsperarOrden(), 
            transitions={'base': waypoints[0][0], 'mostrador': waypoints[1][0], 'habitacion1': waypoints[2][0], 
            'habitacion2': waypoints[3][0], 'habitacion3': waypoints[4][0], 'aborted': 'EsperarOrden'}, 
            remapping={'input': 'bateria', 'output': 'bateria'})

            # base
            StateMachine.add(waypoints[0][0], HacerRuta(),transitions={'1': 'succeeded', '0': 'aborted', 'repeat': waypoints[0][0]},
            remapping={'input': 'bateria', 'output': 'bateria'})

            # mostrador
            StateMachine.add(waypoints[1][0], HacerRuta(waypoints[1][1], waypoints[1][2], waypoints[1][0]),
                             transitions={'succeeded': 'succeeded', 'aborted': 'EsperarOrden'})

            # habitacion1
            StateMachine.add(waypoints[2][0], HacerRuta(waypoints[2][1], waypoints[2][2], waypoints[2][0]),
                             transitions={'succeeded': 'succeeded', 'aborted': 'EsperarOrden'})

            # habitacion2
            StateMachine.add(waypoints[3][0], HacerRuta(waypoints[3][1], waypoints[3][2], waypoints[3][0]),
                             transitions={'succeeded': 'succeeded', 'aborted': 'EsperarOrden'})

            # habitacion3
            StateMachine.add(waypoints[4][0], HacerRuta(waypoints[4][1], waypoints[4][2], waypoints[4][0]),
                             transitions={'succeeded': 'succeeded', 'aborted': 'EsperarOrden'})

            # habitacion4
            StateMachine.add(waypoints[5][0], HacerRuta(waypoints[5][1], waypoints[5][2], waypoints[5][0]),
                             transitions={'succeeded': 'succeeded', 'aborted': 'EsperarOrden'})

        sis = IntrospectionServer('server_name', sm, '/SM_ROOT')
        sis.start()
        # Ejecutamos la maquina de estados
        sm_ejecutada = sm.execute()
        rospy.spin()
        sis.stop()


def callbackServicio(data):
    rospy.loginfo("Se ha llamado al servicio /nursie_navigation_fsm")
    ubi = data.ubicacion
    controlador = main(ubi)
    respuesta = Ubicacion_MsgResponse()
    respuesta.response = True

    return respuesta


# Servicio
rospy.init_node('nursie_navigation_fsm', anonymous=False)
servicio = rospy.Service('/nursie_navigation_fsm',
                         Ubicacion_Msg, callbackServicio)
rospy.Rate(1)
rospy.loginfo("El servicio esta listo")
rospy.spin()
